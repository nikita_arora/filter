package com.example.nikitaarora.assignment_recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.nikitaarora.assignment_recyclerview.MainActivity.Data;

import java.util.ArrayList;

/**
 * Created by nikita.arora on 12/20/2015.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Data> stringList;

    public RecyclerAdapter(ArrayList<Data> data)
    {
        this.stringList = data;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder mViewHolder ;

            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.list_row, parent, false);

            mViewHolder = new TextViewHolder(v);

            return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof TextViewHolder) {
            switch (position % 2) {
                case 1:

                    ((TextViewHolder) holder).odd.setVisibility(View.VISIBLE);
                    ((TextViewHolder) holder).odd.setText(stringList.get(position).str);

                    ((TextViewHolder) holder).text.setVisibility(View.GONE);
                    ((TextViewHolder) holder).chk.setVisibility(View.GONE);
                    break;

                case 0:
                    ((TextViewHolder) holder).text.setVisibility(View.VISIBLE);
                    ((TextViewHolder) holder).chk.setVisibility(View.VISIBLE);

                    ((TextViewHolder) holder).odd.setVisibility(View.GONE);

                    ((TextViewHolder) holder).text.setText(stringList.get(position).str);
                    ((TextViewHolder) holder).chk.setChecked(stringList.get(position).chk);

                    ((TextViewHolder) holder).chk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            stringList.get(position).chk = !(stringList.get(position).chk);
                        }
                    });
                    break;

            }
        }
        }

    @Override
    public int getItemCount() {
        return stringList.size();
    }



    public static class TextViewHolder extends RecyclerView.ViewHolder {
        protected TextView odd;
        protected TextView text;
        protected CheckBox chk;

        public TextViewHolder(View itemView) {
            super(itemView);

            this.odd = (TextView) itemView.findViewById(R.id.text_left);
            this.text = (TextView) itemView.findViewById(R.id.text_right);
            this.chk = (CheckBox) itemView.findViewById(R.id.check);

        }
    }

}
