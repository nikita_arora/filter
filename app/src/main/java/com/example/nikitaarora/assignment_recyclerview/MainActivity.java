package com.example.nikitaarora.assignment_recyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final ArrayList<Data> data= new ArrayList<>();
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.loadData();

        mRecyclerView = (RecyclerView) findViewById(R.id.list);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new RecyclerAdapter(data);
        mRecyclerView.setAdapter(mAdapter);

        }



    private ArrayList<Data> loadData() {

        for (int i = 0; i < 10000; i++) {
            if (i % 2 == 0) {
                String msg = "This is even row";
                Data obj = new Data(msg, false);
                data.add(i, obj);
            } else {
                String msg = "This is odd row";
                Data obj1 = new Data(msg, false);
                data.add(i, obj1);
            }
        }
        return data;
    }


    public static class Data
    {
        String str;
        boolean chk;

        public Data(String msg, boolean value)
        {
            this.str = msg;
            this.chk = value;
        }
    }

}


